let number = prompt("Enter any number greater than 50: ");
console.log("The number you provided is " + number);
for(let i = number;i>=0;i--){
	if (i <= 50){
		console.log("The current value is at " + i + ". Terminating the loop");
		break;
	}
	if (i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if (i % 5 === 0){
		console.log(i);
	}
}

let stringContainer = "supercalifragilisticexpialidocious";
let consonantsContainer = "";


for(i=0;i<stringContainer.length;i++){
	if(stringContainer[i] === "a" || stringContainer[i] === "e" || stringContainer[i] === "i" || stringContainer[i] === "o" || stringContainer[i] === "u"){
		continue;
	}
	else {
		consonantsContainer = consonantsContainer + stringContainer[i];
	}

}
console.log(stringContainer);
console.log(consonantsContainer);
